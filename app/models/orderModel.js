//import thư viện mongoose
const mongoose = require("mongoose");

//khai báo Schema
const Schema = mongoose.Schema;

//khai báo module order
const orderSchema = new Schema({
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: Schema.Types.ObjectId, ref: "voucher"
    },
    drink: {
        type: Schema.Types.ObjectId, ref: "drink"
    },
    status: {
        type: String,
        required: true
    }
}, {
    timestamps: true 
});


module.exports = mongoose.model("order", orderSchema);